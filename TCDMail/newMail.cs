﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using mshtml;
using System.IO;
using System.Diagnostics;

namespace TCDMail
    {
    public partial class NewMail : Form
        {
        public static IHTMLDocument2 doc;
        private List<MailMan> Accounts;
        private static string emptyHTML = "<html><head></head><body></body></html>";
        public NewMail(List<MailMan> Accounts)
            {
            InitializeComponent();
            this.Accounts = Accounts;
            htmlEditor.DocumentText = emptyHTML;
            doc = htmlEditor.Document.DomDocument as IHTMLDocument2;
            doc.designMode = "On";
            foreach (MailMan Account in Accounts)
                {
                Debug.Write(Account.Name + "\n");
                accountsComboBox.Items.Clear();
                accountsComboBox.Items.Add(Account.Name);
                }
            }

        private void sendToolStripMenuItem_Click(object sender, EventArgs e)
            {

            }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
            {
            htmlEditor.DocumentText = emptyHTML;
            }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
            {
            openFileDialog1.ShowDialog();
            }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
            {
            string filename = openFileDialog1.FileName;
            string content = File.ReadAllText(filename);
            IHTMLTxtRange range = doc.selection.createRange() as IHTMLTxtRange;
            range.pasteHTML(content);
            range.collapse(false);
            range.select();
            }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
            {
            string filename = saveFileDialog1.FileName;
            string content = htmlEditor.DocumentText;
            File.WriteAllText(filename, content);
            }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
            {
            doc.execCommand("Cut", false, null);
            }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
            {
            doc.execCommand("Copy", false, null);
            }

        private void toolStripMenuItem1_Click_1(object sender, EventArgs e)
            {
            doc.execCommand("Undo", false, null);
            }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
            {
            doc.execCommand("Paste", false, null);
            }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
            {
            doc.execCommand("Redo", false, null);
            }

        private void allToolStripMenuItem_Click(object sender, EventArgs e)
            {
            //doc.execCommand("Select All", false, null);
            //not works
            }

        private void button1_Click(object sender, EventArgs e)
            {
            //left align
            doc.execCommand("justifyLeft", false, null);
            }

        private void centerBt_Click(object sender, EventArgs e)
            {
            doc.execCommand("justifyCenter", false, null);
            }

        private void button3_Click(object sender, EventArgs e)
            {
            doc.execCommand("justifyRight", false, null);
            }

        private void justifyBt_Click(object sender, EventArgs e)
            {
            doc.execCommand("justifyFull", false, null);
            }

        private void boldToolStripMenuItem_Click(object sender, EventArgs e)
            {
            doc.execCommand("Bold", false, null);
            }

        private void italicToolStripMenuItem_Click(object sender, EventArgs e)
            {
            doc.execCommand("Italic", false, null);
            }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
            {
            doc.execCommand("Strike Through", false, null);
            }

        private void strikeToolStripMenuItem_Click(object sender, EventArgs e)
            {
            doc.execCommand("Underline", false, null);
            }

        private void groupBox1_Enter(object sender, EventArgs e)
            {

            }

        private void openToolStripMenuItem1_Click(object sender, EventArgs e)
            {
            openFileDialog1.ShowDialog();
            }

        private void sendButton_Click(object sender, EventArgs e)
            {
            
            string content = this.htmlEditor.DocumentText;
            string rcpts = this.recipientsTb.Text;
            List<String> rcpArr = rcpts.Split(';').ToList();
            MailSender selectedAccount = (MailSender)Accounts[accountsComboBox.SelectedIndex + 1];
            selectedAccount.Send(rcpArr, ccTb.Text, null, content);
            
            //mailer.Send(rcpArr, ccTb.Text, null, this.subjectTb.Text, content);
            }
        }
    }
