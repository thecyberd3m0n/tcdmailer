﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TCDMail
    {
    public partial class Form1 : Form
        {
        private NewMail newMailForm;
        public List<MailMan> Accounts;
        public Form1()
            {
            InitializeComponent();
            Accounts = new List<MailMan>();
            }

        private void senderButton_Click(object sender, EventArgs e)
            {
            newMailForm = new NewMail(this.Accounts);
            newMailForm.Show();
            }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
            {
            Application.Exit();
            }

        private void accountsToolStripMenuItem_Click(object sender, EventArgs e)
            {

            }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
            {
            AddAcount AccountAdder = new AddAcount(this);
            AccountAdder.Show();
            }
        private void refreshToolstrip()
            {
            //delete all accounts
            accountsToolStripMenuItem.DropDownItems.RemoveAt(1);
            accountsToolStripMenuItem.DropDownItems.Add(new ToolStripSeparator());
            //now add
            foreach (MailMan Account in Accounts)
                {
                ToolStripMenuItem item = new ToolStripMenuItem();
                item.Text = Account.Name;
                item.Click += new EventHandler(item_Click);
                accountsToolStripMenuItem.DropDownItems.Add(item);
                }
            }
        private void item_Click(object sender, EventArgs e)
            {

            }
        public void addAccountToList(MailMan mail)
            {
            Accounts.Add(mail);
            refreshToolstrip();
            }

        }
    }
