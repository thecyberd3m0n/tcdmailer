﻿namespace TCDMail
    {
    partial class AddAcount
        {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
            {
            if (disposing && (components != null))
                {
                components.Dispose();
                }
            base.Dispose(disposing);
            }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
            {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label9 = new System.Windows.Forms.Label();
            this.pop3PortTb = new System.Windows.Forms.TextBox();
            this.pop3PasswordTb = new System.Windows.Forms.TextBox();
            this.pop3UsernameTb = new System.Windows.Forms.TextBox();
            this.pop3MailTb = new System.Windows.Forms.TextBox();
            this.pop3HostTb = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.smtpPortTb = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.smtpAuthCb = new System.Windows.Forms.CheckBox();
            this.smtpPasswordTb = new System.Windows.Forms.TextBox();
            this.smtpUsernameTb = new System.Windows.Forms.TextBox();
            this.smtpMailTb = new System.Windows.Forms.TextBox();
            this.smtpHostTb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.saveAccountButton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.accountNameTb = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 33);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(413, 288);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.Leave += new System.EventHandler(this.tabControl1_Leave);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.LightGray;
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.pop3PortTb);
            this.tabPage1.Controls.Add(this.pop3PasswordTb);
            this.tabPage1.Controls.Add(this.pop3UsernameTb);
            this.tabPage1.Controls.Add(this.pop3MailTb);
            this.tabPage1.Controls.Add(this.pop3HostTb);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(405, 262);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "POP3";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(26, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Port";
            // 
            // pop3PortTb
            // 
            this.pop3PortTb.Location = new System.Drawing.Point(257, 41);
            this.pop3PortTb.Name = "pop3PortTb";
            this.pop3PortTb.Size = new System.Drawing.Size(142, 20);
            this.pop3PortTb.TabIndex = 8;
            // 
            // pop3PasswordTb
            // 
            this.pop3PasswordTb.Location = new System.Drawing.Point(257, 136);
            this.pop3PasswordTb.Name = "pop3PasswordTb";
            this.pop3PasswordTb.Size = new System.Drawing.Size(142, 20);
            this.pop3PasswordTb.TabIndex = 7;
            this.pop3PasswordTb.Leave += new System.EventHandler(this.pop3PasswordTb_Leave);
            // 
            // pop3UsernameTb
            // 
            this.pop3UsernameTb.Location = new System.Drawing.Point(257, 104);
            this.pop3UsernameTb.Name = "pop3UsernameTb";
            this.pop3UsernameTb.Size = new System.Drawing.Size(142, 20);
            this.pop3UsernameTb.TabIndex = 6;
            this.pop3UsernameTb.Leave += new System.EventHandler(this.pop3UsernameTb_Leave);
            // 
            // pop3MailTb
            // 
            this.pop3MailTb.Location = new System.Drawing.Point(257, 74);
            this.pop3MailTb.Name = "pop3MailTb";
            this.pop3MailTb.Size = new System.Drawing.Size(142, 20);
            this.pop3MailTb.TabIndex = 5;
            this.pop3MailTb.Leave += new System.EventHandler(this.pop3MailTb_Leave);
            // 
            // pop3HostTb
            // 
            this.pop3HostTb.Location = new System.Drawing.Point(257, 9);
            this.pop3HostTb.Name = "pop3HostTb";
            this.pop3HostTb.Size = new System.Drawing.Size(142, 20);
            this.pop3HostTb.TabIndex = 4;
            this.pop3HostTb.Leave += new System.EventHandler(this.pop3HostTb_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 139);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Username";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "E-mail";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Host";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.LightGray;
            this.tabPage2.Controls.Add(this.smtpPortTb);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.smtpAuthCb);
            this.tabPage2.Controls.Add(this.smtpPasswordTb);
            this.tabPage2.Controls.Add(this.smtpUsernameTb);
            this.tabPage2.Controls.Add(this.smtpMailTb);
            this.tabPage2.Controls.Add(this.smtpHostTb);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(405, 262);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "SMTP";
            // 
            // smtpPortTb
            // 
            this.smtpPortTb.Location = new System.Drawing.Point(257, 40);
            this.smtpPortTb.Name = "smtpPortTb";
            this.smtpPortTb.Size = new System.Drawing.Size(142, 20);
            this.smtpPortTb.TabIndex = 19;
            this.smtpPortTb.Text = "587";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 43);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Port";
            // 
            // smtpAuthCb
            // 
            this.smtpAuthCb.AutoSize = true;
            this.smtpAuthCb.Location = new System.Drawing.Point(9, 239);
            this.smtpAuthCb.Name = "smtpAuthCb";
            this.smtpAuthCb.Size = new System.Drawing.Size(127, 17);
            this.smtpAuthCb.TabIndex = 17;
            this.smtpAuthCb.Text = "SMTP Authentication";
            this.smtpAuthCb.UseVisualStyleBackColor = true;
            // 
            // smtpPasswordTb
            // 
            this.smtpPasswordTb.Location = new System.Drawing.Point(257, 137);
            this.smtpPasswordTb.Name = "smtpPasswordTb";
            this.smtpPasswordTb.Size = new System.Drawing.Size(142, 20);
            this.smtpPasswordTb.TabIndex = 15;
            // 
            // smtpUsernameTb
            // 
            this.smtpUsernameTb.Location = new System.Drawing.Point(257, 105);
            this.smtpUsernameTb.Name = "smtpUsernameTb";
            this.smtpUsernameTb.Size = new System.Drawing.Size(142, 20);
            this.smtpUsernameTb.TabIndex = 14;
            // 
            // smtpMailTb
            // 
            this.smtpMailTb.Location = new System.Drawing.Point(257, 75);
            this.smtpMailTb.Name = "smtpMailTb";
            this.smtpMailTb.Size = new System.Drawing.Size(142, 20);
            this.smtpMailTb.TabIndex = 13;
            // 
            // smtpHostTb
            // 
            this.smtpHostTb.Location = new System.Drawing.Point(257, 9);
            this.smtpHostTb.Name = "smtpHostTb";
            this.smtpHostTb.Size = new System.Drawing.Size(142, 20);
            this.smtpHostTb.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Password";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Username";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 78);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "E-mail";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Host";
            // 
            // saveAccountButton
            // 
            this.saveAccountButton.Location = new System.Drawing.Point(346, 327);
            this.saveAccountButton.Name = "saveAccountButton";
            this.saveAccountButton.Size = new System.Drawing.Size(75, 23);
            this.saveAccountButton.TabIndex = 18;
            this.saveAccountButton.Text = "Save";
            this.saveAccountButton.UseVisualStyleBackColor = true;
            this.saveAccountButton.Click += new System.EventHandler(this.saveAccountButton_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(265, 327);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 19;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(79, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Account name:";
            // 
            // accountNameTb
            // 
            this.accountNameTb.Location = new System.Drawing.Point(265, 7);
            this.accountNameTb.Name = "accountNameTb";
            this.accountNameTb.Size = new System.Drawing.Size(156, 20);
            this.accountNameTb.TabIndex = 21;
            // 
            // AddAcount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 358);
            this.Controls.Add(this.accountNameTb);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.saveAccountButton);
            this.Controls.Add(this.tabControl1);
            this.Name = "AddAcount";
            this.Text = "AddAcount";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

            }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox pop3PasswordTb;
        private System.Windows.Forms.TextBox pop3UsernameTb;
        private System.Windows.Forms.TextBox pop3MailTb;
        private System.Windows.Forms.TextBox pop3HostTb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox smtpPasswordTb;
        private System.Windows.Forms.TextBox smtpUsernameTb;
        private System.Windows.Forms.TextBox smtpMailTb;
        private System.Windows.Forms.TextBox smtpHostTb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox smtpAuthCb;
        private System.Windows.Forms.Button saveAccountButton;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox pop3PortTb;
        private System.Windows.Forms.TextBox smtpPortTb;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox accountNameTb;
        }
    }