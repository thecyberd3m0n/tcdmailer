﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Diagnostics;

namespace TCDMail
    {
    public partial class AddAcount : Form
        {
        private Form1 f;
        public AddAcount(Form1 p_form)
            {
            InitializeComponent();
            f = p_form;
            }

        private void pop3HostTb_Leave(object sender, EventArgs e)
            {
            smtpHostTb.Text = pop3HostTb.Text;
            }

        private void button2_Click(object sender, EventArgs e)
            {
            this.Close();
            }

        private void tabControl1_Leave(object sender, EventArgs e)
            {

            }

        private void pop3UsernameTb_Leave(object sender, EventArgs e)
            {
            smtpUsernameTb.Text = pop3UsernameTb.Text;
            }

        private void pop3PasswordTb_Leave(object sender, EventArgs e)
            {
            smtpPasswordTb.Text = pop3PasswordTb.Text;
            }

        private void pop3MailTb_Leave(object sender, EventArgs e)
            {
            smtpUsernameTb.Text = pop3UsernameTb.Text;
            }

        private void saveAccountButton_Click(object sender, EventArgs e)
            {
            if (validateData() && accountNameTb.Text != "")
                {
                MailMan mail = new MailMan(accountNameTb.Text);
                mail.SetSmtp(smtpHostTb.Text, Convert.ToInt16(smtpPortTb.Text), smtpMailTb.Text, smtpUsernameTb.Text,smtpPasswordTb.Text,false,false);
                f.addAccountToList(mail);
                this.Close();
                }
            else
                {
                MessageBox.Show("Invalid data, or connection problem!!", "Error");
                }
            }
        private bool validateData()
            {
            RegexUtilities validator = new RegexUtilities();
            Int16 smtpPort;
            if (!(validator.IsValidEmail(smtpMailTb.Text))) return false;
            try
                {
                smtpPort = Int16.Parse(smtpPortTb.Text);
                
                }
            catch (FormatException)
                {
                return false;
                }
            //PingClass ping = new PingClass();
            //for (int i = 0; i < 5;i++ )
            //    {
            //    if (ping.testServer(smtpHostTb.Text, 300)) break;
            //    if (i == 4)
            //        {
            //            Debug.Write("Ping test failed - server not exist, not active or other connection problem");
            //            return false;
            //        }
            //    }
                return true;
            }

        }
    }
