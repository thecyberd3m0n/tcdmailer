﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.NetworkInformation;
using System.Diagnostics;

class PingClass
    {

    private PingOptions options;
    private string data;
    private byte[] buffer;
    private Int16 timeout;


    public PingClass()
        {
        // TODO: Complete member initialization

        //DEFINE PING

        this.options = new PingOptions();

        this.options.DontFragment = true;
        for (int i = 0; i < 32; i++)
            {
            this.data += "F";
            }
        this.buffer = Encoding.ASCII.GetBytes(data);


        }

    public bool testServer(string serverAddress, Int16 timeOut)
        {
        
        this.timeout = timeOut;
        Ping pingSender = new Ping();
        try
            {
            PingReply reply = pingSender.Send(serverAddress, timeout, buffer, options);


            if (reply.Status != IPStatus.Success)
                {
                return false;
                }
            
            }
        catch (PingException e)
            {
            Debug.Write(e.ToString());
            return false;
            }
        return true;
        }
    }
    

