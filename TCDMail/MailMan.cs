﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.ComponentModel;
using System.Diagnostics;

namespace TCDMail
    {
    public class MailMan
        {
        public String Name { get; set; }
        public MailAddress fromAddress { get; set; }
        protected SmtpClient smtp;

        public MailMan()
            {

            }

        public MailMan(String Name)
            {
            this.Name = Name;
            }

        public void SetSmtp(string smtpHost, Int16 port, string from, string user, string password, bool EnableSsl, bool defaultCredentials = true)
            {
            //SMTP Object Create
           
            this.fromAddress = new MailAddress(from);
            smtp = new SmtpClient(smtpHost,port);
            if (!defaultCredentials)
                {
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(user, password);
                }
            else
                {
                smtp.UseDefaultCredentials = true;
                }

            }

        }

    class MailSender : MailMan
        {
        public void Send(List<String> addressList, String copyTo, string UDW, string subject = null, string content = null, bool isHtml = true)
            {
            MailMessage email = new MailMessage();

            foreach (string address in addressList)
                {
                email.To.Add(address);
                }
            email.Subject = subject;
            email.SubjectEncoding = System.Text.Encoding.UTF8;
            email.IsBodyHtml = isHtml;
           
            email.Body = content;
            email.From = fromAddress;
            if (copyTo != null && copyTo != "")
                {
                try
                    {
                    MailAddress copy = new MailAddress(copyTo);
                    email.CC.Add(copy);
                    }
                catch (FormatException)
                    {
                    throw new FormatException("Address invalid");
                    }
                }

            smtp.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);

            try
                {
                smtp.SendAsync(email, uniqid("", false));
                }
            catch (SmtpException e)
                {
                throw e.InnerException;
                }
            }

        private string uniqid(string prefix, bool more_entropy)
            {
            if (string.IsNullOrEmpty(prefix))
                prefix = string.Empty;

            if (!more_entropy)
                {
                return (prefix + System.Guid.NewGuid().ToString()).Substring(13);
                }
            else
                {
                return (prefix + System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString()).Substring(23);
                }
            }

        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
            {
            String token = (string)e.UserState;

            if (e.Error != null)
                {
                throw new Exception(e.Error.ToString());
                }
            else
                {
                Debug.Write("Message sent");
                }
            }
        }

    class POP3MailReceiver : MailMan
        {

        }
    }
